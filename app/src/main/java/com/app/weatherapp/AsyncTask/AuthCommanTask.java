package com.app.weatherapp.AsyncTask;

import android.content.Context;
import android.text.TextUtils;
import android.widget.ProgressBar;

import com.app.weatherapp.Constants.StaticConstants;
import com.app.weatherapp.Exception.SearchException;
import com.app.weatherapp.Network.BaseNetwork;
import com.app.weatherapp.Interface.IAsyncTaskRunner;
import com.app.weatherapp.Network.ResultMessage;
import com.app.weatherapp.Network.WeatherAppParser;
import com.app.weatherapp.Util.Logger;

import java.util.HashMap;


/**
 * This is the task used for parsing data for every end point.
 * 
 * @author Bhawna Verma
 * 
 */
public class AuthCommanTask<Parems, Result> extends
AsyncTaskRunner<Parems, Object, Result> {
	String UserId = "";
	WeatherAppParser jsonParser;
	String MEthodtype;
	HashMap<String, String> paramsHashmap = new HashMap<>();
	private int position;

	public AuthCommanTask(Context context,
						  IAsyncTaskRunner<Object, Result> asyncTaskRunner, String Keyword,
						  ProgressBar Progressloader, String methodtype) {
		super(context, Keyword, Progressloader, asyncTaskRunner);
		this.MEthodtype = methodtype;
	}

	public AuthCommanTask(Context context,
			IAsyncTaskRunner<Object, Result> asyncTaskRunner, String Keyword,
			String methodtype) {
		super(context, Keyword, asyncTaskRunner);
		this.MEthodtype = methodtype;
	}

	public AuthCommanTask(Context context,
			IAsyncTaskRunner<Object, Result> asyncTaskRunner, String Keyword,
			String UserId, String methodtype) {
		super(context, Keyword, asyncTaskRunner);
		this.UserId = UserId;
		this.MEthodtype = methodtype;
	}



	public AuthCommanTask(Context context,
			IAsyncTaskRunner<Object, Result> asyncTaskRunner, String Keyword,
			ProgressBar Progressloader, String methodtype, int position) {
		super(context, Keyword, Progressloader, asyncTaskRunner);
		this.MEthodtype = methodtype;
		this.position = position;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected Result doInBackground(Parems... params) {
		ResultMessage resultMessage = new ResultMessage();
		resultMessage.STATUS = StaticConstants.ASYN_NETWORK_FAIL;
		if (!BaseNetwork.obj().checkConnOnline(context))
			return (Result) resultMessage;

		resultMessage.TYPE = urlString;
		paramsHashmap = (HashMap<String, String>) params[0];
		jsonParser = new WeatherAppParser();
		jsonString = BaseNetwork.obj().PostMethodWay(context, urlString,
				(HashMap<String, String>) params[0], BaseNetwork.obj().TimeOut,
				this.MEthodtype);


		Logger.d("AuthLoginTask--Response", "::" + jsonString);
		if (!TextUtils.isEmpty(jsonString)) {

			if (urlString.equalsIgnoreCase(BaseNetwork.obj().KEY_FORECAST)) {
				jsonParser.ParseForecastData(context, resultMessage, jsonString);
			}



		} else {
			resultMessage.STATUS = SearchException.SERVER_DELAY;
			return (Result) resultMessage;
		}
		return (Result) resultMessage;
	}

	@Override
	protected void onProgressUpdate(Object... progress) {
		super.onProgressUpdate(progress);
	}

	@Override
	protected void onPostExecute(Result result) {
		super.onPostExecute(result);
	}

}
