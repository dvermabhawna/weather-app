package com.app.weatherapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.app.weatherapp.Constants.PrefHelper;
import com.app.weatherapp.Constants.StaticConstants;
import com.app.weatherapp.CurrentWeatherFragment.CurrentWeather;
import com.app.weatherapp.Drawer.DrawerAdapter;
import com.app.weatherapp.Drawer.DrawerModel;
import com.app.weatherapp.ForeCastWeatherFragment.FiveDayForecastWeather;
import com.app.weatherapp.Interface.ILogoutConfirm;
import com.app.weatherapp.Interface.OnBackPressListener;
import com.app.weatherapp.Ui.LogoutDialog;
import com.app.weatherapp.Ui.MenuDrawer;
import com.app.weatherapp.Util.Util;

import java.util.ArrayList;

/**
 * Created by Bhawna on 3/26/2018.
 */

public class BaseFragmentActivity extends FragmentActivity implements
        View.OnClickListener, ILogoutConfirm {
    OnBackPressListener currentBackListener;
    private MenuDrawer slide_me;
    private ListView list_drawer;
    private ArrayList<DrawerModel> modulelist;
    private DrawerAdapter adapter;
    private LinearLayoutManager linearLayoutManager;
    private RecyclerView mRecyclerView;
    private ImageView img_menuIcon;
    private TextView text_mainHeader;
    private ImageView img_refreshIcon;
    private String unitType = "\u2103";
    private TextView text_versionNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_basefragment);
        modulelist = Util.getModuleItems(this);
        initview();

    }

    private void initview() {
        img_menuIcon = (ImageView) findViewById(R.id.img_menuIcon);
        img_refreshIcon = (ImageView) findViewById(R.id.img_refreshIcon);
        text_mainHeader = (TextView) findViewById(R.id.text_mainHeader);
        slide_me = new MenuDrawer(this);
        slide_me.setLeftBehindContentView(R.layout.drawer_listview);
        adapter = new DrawerAdapter(modulelist, this);
        linearLayoutManager = new LinearLayoutManager(this, OrientationHelper.VERTICAL, false);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        text_versionNumber = (TextView) findViewById(R.id.text_versionNumber);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(adapter);
        img_menuIcon.setOnClickListener(this);
        img_refreshIcon.setOnClickListener(this);
        text_versionNumber.setText("Version No.-" + Util.getVersionNumber(this));
        OnCurrentWeatherFragment(true);
    }

    public void OnCurrentWeatherFragment(boolean openDrawer) {
        if (openDrawer)
            slide_me.toggleLeftDrawer();
        text_mainHeader.setText(StaticConstants.CURRENT_WEATHER_HEADING);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        CurrentWeather login = new CurrentWeather();
        ft.replace(R.id.frame_baseContainer, login, StaticConstants.FRAGMENT_CURRENTWEATHER_TAG);
        currentBackListener = login;
        ft.commit();
    }

    public void OnForecastWeatherFragment(boolean openDrawer) {
        if (openDrawer)
            slide_me.toggleLeftDrawer();
        text_mainHeader.setText(StaticConstants.FIVE_DAY_FORECAST_HEADING);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        FiveDayForecastWeather login = new FiveDayForecastWeather();
        ft.replace(R.id.frame_baseContainer, login, StaticConstants.FRAGMENT_FORECASTWEATHER_TAG);
        currentBackListener = login;
        ft.commit();
    }

    public void LogoutCall() {
        LogoutDialog logoutDialog = new
                LogoutDialog(this, this
        );
        logoutDialog.show();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_menuIcon:
                slide_me.toggleLeftDrawer();
                break;
            case R.id.img_refreshIcon:
                Fragment fragment = getSupportFragmentManager().findFragmentById(
                        R.id.frame_baseContainer);
                if (fragment.getTag().toString()
                        .equalsIgnoreCase(StaticConstants.FRAGMENT_CURRENTWEATHER_TAG)) {
                    CurrentWeather currentWeather = (CurrentWeather) getSupportFragmentManager()
                            .findFragmentById(R.id.frame_baseContainer);
                    currentWeather.CallWeatherService(unitType);
                } else if (fragment.getTag().toString()
                        .equalsIgnoreCase(StaticConstants.FRAGMENT_FORECASTWEATHER_TAG)) {
                    FiveDayForecastWeather fiveDayForecastWeather = (FiveDayForecastWeather) getSupportFragmentManager()
                            .findFragmentById(R.id.frame_baseContainer);
                    fiveDayForecastWeather.getFiveDayForecastData();
                }
                break;
        }
    }

    @Override
    public void LogMeOut() {
        PrefHelper.ClearAll(this, PrefHelper.PREF_FILE_NAME);
        startActivity(new Intent
                (this, LoginActivity.class));
        finish();
    }

    @Override
    public void onBackPressed() {
        if (currentBackListener != null
                && currentBackListener.onBackPressed() == true) {
            return;
        } else {
            finish();
        }
    }
}
