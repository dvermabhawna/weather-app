package com.app.weatherapp.Constants;

/**Class to handle shared prefrence file.
 * @author Bhawna Verma
 *
 */
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class PrefHelper{

	public static String PREF_FILE_NAME = "Weather_PREF_FILE";
	public static String PREF_FILE_NAME_ONLY = "Weather_PREF_FILE_ONLY";


	static public String getStoredString(Context aContext, String FileName,
			String aKey) {
		SharedPreferences sharedPrefs = aContext.getSharedPreferences(FileName,
				0);
		return sharedPrefs.getString(aKey, "");
	}

	static public float getStoredFloat(Context aContext, String FileName,
			String aKey) {
		SharedPreferences sharedPrefs = aContext.getSharedPreferences(FileName,
				0);
		return sharedPrefs.getFloat(aKey, 0.0f);
	}

	static public boolean getStoredBoolean(Context aContext, String FileName,
			String aKey) {
		SharedPreferences sharedPrefs = aContext.getSharedPreferences(FileName,
				0);
		return sharedPrefs.getBoolean(aKey, true);
	}

	static public int getStoredInt(Context aContext, String FileName,
			String aKey) {
		SharedPreferences sharedPrefs = aContext.getSharedPreferences(FileName,
				0);
		return sharedPrefs.getInt(aKey, 0);
	}

	static public void storeString(Context aContext, String FileName,
			String aKey, String aValue) {
		SharedPreferences sharedPrefs = aContext.getSharedPreferences(FileName,
				0);
		Editor editor = sharedPrefs.edit();
		editor.putString(aKey, aValue);
		editor.commit();
	}

	static public void storeInt(Context aContext, String FileName, String aKey,
			int aValue) {
		SharedPreferences sharedPrefs = aContext.getSharedPreferences(FileName,
				0);
		Editor editor = sharedPrefs.edit();
		editor.putInt(aKey, aValue);
		editor.commit();
	}
	
	static public void storeFloat(Context aContext, String FileName, String aKey,
			float aValue) {
		SharedPreferences sharedPrefs = aContext.getSharedPreferences(FileName,
				0);
		Editor editor = sharedPrefs.edit();
		editor.putFloat(aKey, aValue);
		editor.commit();
	}

	static public void storeDouble(Context aContext, String FileName,
			String aKey, double aValue) {
		SharedPreferences sharedPrefs = aContext.getSharedPreferences(FileName,
				0);
		Editor editor = sharedPrefs.edit();
		editor.putFloat(aKey, (float) aValue);
		editor.commit();
	}

	static public void storeBoolean(Context aContext, String FileName,
			String aKey, boolean aValue) {
		SharedPreferences sharedPrefs = aContext.getSharedPreferences(FileName,
				0);
		Editor editor = sharedPrefs.edit();
		editor.putBoolean(aKey, aValue);
		editor.commit();
	}
	static public void ClearAll(Context aContext, String FileName){
		SharedPreferences sharedPrefs = aContext.getSharedPreferences(FileName,
				0);
		Editor editor = sharedPrefs.edit();
		editor.clear();
		editor.commit();
	}
	static public boolean getStoredBooleanG(Context aContext, String FileName,
											String aKey) {
		SharedPreferences sharedPrefs = aContext.getSharedPreferences(FileName,
				0);
		return sharedPrefs.getBoolean(aKey, false);
	}

}
