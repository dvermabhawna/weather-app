package com.app.weatherapp.Constants;

/**Class to maintain all static fields at one place so that we can change it at one place.
 * @author Bhawna Verma
 *
 */

public class StaticConstants {

    public static final int ASYN_NETWORK_FAIL = 0;
    public static final int ASYN_RESULT_OK = 1;
    public static final int ASYN_RESULT_CANCEL = 3;
    public static final int ASYN_RESULT_LOGINFAILURE= 9;
    public static final int ASYN_RESULT_OK_ = 4;
    public static final int ASYN_RESULT_PARSE_FAILED = 5;
    public static final int ASYN_RESULT_DRAFTCANCEL = 6;

    public static final String POST_METHOD = "POST";
    public static final String GET_METHOD = "GET";
    public static final String DELETE_METHOD = "DELETE";
    public static final String JSON_TAG_ERROR = "error";
    public static final String JSON_TAG_AUTHENTICATION = "authorization";
    public static final String JSON_TAG_AUTH_TRUE = "true";
    public static final String JSON_TAG_OK = "OK";
    public static final String JSON_TAG_AUTH_FALSE = "false";
    public static final String JSON_TAG_MESSAGE = "message";
    public static final String JSON_TAG_CODE = "code";

    public static final String FACEBOOK_USER_ID="facebook_user_id";
    public static final String FACEBOOK_USER_NAME="facebook_user_name";
    public static final String FACEBOOK_USER_IMAGE="facebook_user_image";


    public static final String FRAGMENT_CURRENTWEATHER_TAG = "current_weather_tag";
    public static final String FRAGMENT_FORECASTWEATHER_TAG = "forecast_weather_tag";
    public static final String MENU_CURRENT_WEATHER = "Current Weather";
    public static final String MENU_FORECAST = "5 Days Forecast ";
    public static final String MENU_LOGOUT = "Logout";
    public static final String JSON_USER_LATTITUDE ="lat";
    public static final String JSON_FORECAST_UNIT ="units";
    public static final String JSON_USER_LONGITUDE ="lon";
    public static final String JSON_USER_APPID ="appid";
    public static final String FORECAST_LIST ="list";
    public static final String FORECAST_MAIN ="main";
    public static final String JSON_DATE_TIME ="dt_txt";

    public static final String FORECAST_LIST_DATA = "forecast_list_data";
    public static final String JSON_TEMP = "temp";
    public static final String FORECAST_CITY = "city";
    public static final String JSON_NAME = "name";
    public static final String CURRENT_WEATHER_HEADING = "Current Weather";
    public static final String FIVE_DAY_FORECAST_HEADING = "5 Days Forecast";
}
