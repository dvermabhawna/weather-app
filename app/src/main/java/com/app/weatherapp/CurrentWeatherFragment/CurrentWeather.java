package com.app.weatherapp.CurrentWeatherFragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.app.weatherapp.Interface.OnBackPressListener;
import com.app.weatherapp.Interface.IWeatherInfo;
import com.app.weatherapp.Network.WeatherService;
import com.app.weatherapp.R;
import com.app.weatherapp.Util.TrackGPS;
import com.app.weatherapp.Util.Util;

/**
 * Created by Bhawna on 3/26/2018.
 */

public class CurrentWeather extends Fragment implements OnBackPressListener,
        IWeatherInfo, View.OnClickListener, CurrentWeatherPresenter.View {


    private LayoutInflater inflater;
    private TextView text_currentTemp;
    private Context context;
    private WeatherService mWeatherService;
    private TextView text_currentCity;
    private TrackGPS gps;
    private double mLongitude;
    private double mLatitude;
    private TextView text_farenhite;
    private TextView text_celcius;
    private String unitType = "\u2103";
    private TextView text_currentDate;
    CurrentWeatherPresenter presenter;

    @Override
    public View onCreateView(final LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_currentweather,
                container, false);
        this.inflater = inflater;

        initView(view);
        return view;
    }

    private void initView(View view) {
        GetLocation();
        presenter = new CurrentWeatherPresenter(this);
        text_currentCity = (TextView) view.findViewById(R.id.text_currentCity);
        text_currentDate = (TextView) view.findViewById(R.id.text_currentDate);
        text_currentTemp = (TextView) view.findViewById(R.id.text_currentTemp);
        text_farenhite = (TextView) view.findViewById(R.id.text_farenhite);
        text_celcius = (TextView) view.findViewById(R.id.text_celcius);
        text_currentCity.setOnClickListener(this);
        text_farenhite.setOnClickListener(this);
        text_celcius.setOnClickListener(this);
        text_currentDate.setText(Util.getCurrentDateTime());
        CallWeatherService(unitType);
    }

    public void CallWeatherService(String units) {
        unitType = units;
        text_celcius.setTextColor(ContextCompat.getColor(context, R.color.white));
        text_farenhite.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
        mWeatherService = new WeatherService(this);
        if (Util.isNetworkAvailable(context))
            mWeatherService.getWeather(String.valueOf(mLatitude), String.valueOf(mLongitude), "metric");
        else
            Toast.makeText(context, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onWeatherData(WeatherDataModel data) {
        presenter.updateMain(data.getMain());
        presenter.updateName(data.getName());
        presenter.updateTemp(data.getMain().getTemp());


    }

    @Override
    public void onNoWeatherData() {
        Toast.makeText(context, "Sorry No Weather Data", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.text_celcius:
                unitType = "\u2103";
                text_celcius.setTextColor(ContextCompat.getColor(context, R.color.white));
                text_farenhite.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
                mWeatherService.getWeather(String.valueOf(mLatitude), String.valueOf(mLongitude), "metric");
                break;

            case R.id.text_farenhite:
                unitType = "\u2109";
                text_celcius.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
                text_farenhite.setTextColor(ContextCompat.getColor(context, R.color.white));
                mWeatherService.getWeather(String.valueOf(mLatitude), String.valueOf(mLongitude), "imperial");
                break;
        }
    }

    private void GetLocation() {
        gps = new TrackGPS(context);
        if (gps != null) {
            if (gps.canGetLocation()) {
                mLongitude = gps.getLongitude();
                mLatitude = gps.getLatitude();

            } else {
                gps.showSettingsAlert();
            }
        }


    }


    @Override
    public void updateCityTextView(String info) {
        text_currentCity.setText(info);
    }

    @Override
    public void updateTempTextView(String info) {
        text_currentTemp.setText(info + unitType);
    }
}
