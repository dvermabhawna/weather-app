package com.app.weatherapp.CurrentWeatherFragment;

/**
 * Created by Bhawna on 3/28/2018.
 */

public class CurrentWeatherPresenter {

    WeatherDataModel data;
    WeatherDataModel.MainData mainData;
    View view;

    public CurrentWeatherPresenter(View view) {
        this.view = view;
        data  = new WeatherDataModel();
    }

    public void updateMain(WeatherDataModel.MainData maindata) {
        data.setMain(maindata);

    }
    public void updateName(String fullName) {
        data.setName(fullName);
        view.updateCityTextView(fullName);
    }

    public void updateTemp(String temp) {
        data.getMain().setTemp(temp);
        view.updateTempTextView(temp);
    }

    public interface View {
        void updateCityTextView(String info);
        void updateTempTextView(String info);
    }
}
