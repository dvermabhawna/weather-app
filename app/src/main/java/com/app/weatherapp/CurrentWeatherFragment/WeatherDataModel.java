package com.app.weatherapp.CurrentWeatherFragment;

import org.json.JSONObject;

/**
 * Weather Data object that confirms to JSON provided by
 * http://api.openweathermap.org/data/2.5/weather
 *  Created by Bhawna on 3/26/2018.
 */
public class WeatherDataModel {

    private MainData main;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String main) {
        this.name = main;
    }

    public MainData getMain() {
        return main;
    }

    public void setMain(MainData main) {
        this.main = main;
    }

    public static class MainData {
        private String temp;
        private String pressure;
        private String humidity;

        public String getTemp() {
            return temp;
        }
        public void setTemp(String temp) {
            this.temp = temp;
        }
        public String getPressure() {
            return pressure;
        }
        public void setPressure(String temp) {
            this.pressure = temp;
        }
        public String getHumidity() {
            return humidity;
        }
        public void setHumidity(String temp) {
            this.humidity = temp;
        }

    }
}
