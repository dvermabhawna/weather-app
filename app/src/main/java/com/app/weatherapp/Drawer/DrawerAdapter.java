package com.app.weatherapp.Drawer;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.weatherapp.BaseFragmentActivity;
import com.app.weatherapp.Constants.PrefHelper;
import com.app.weatherapp.Constants.StaticConstants;
import com.app.weatherapp.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
/**
 * Adapter for drawer
 * Created by Bhawna on 3/26/2018.
 */

public class DrawerAdapter extends RecyclerView.Adapter {

    private ArrayList<DrawerModel> dataSet;
    Context mContext;
    int total_types;

    public static class HeaderTypeViewHolder extends RecyclerView.ViewHolder {

        TextView text_userName;
        ImageView img_User;
        public HeaderTypeViewHolder(View itemView) {
            super(itemView);

            this.text_userName = (TextView) itemView.findViewById(R.id.text_userName);
            this.img_User = (ImageView) itemView.findViewById(R.id.img_User);
        }
    }

    public static class ItemTypeViewHolder extends RecyclerView.ViewHolder {

        TextView text_itemName;
        ImageView img_Item;
        ConstraintLayout rel_itemType;

        public ItemTypeViewHolder(View itemView) {
            super(itemView);

            this.text_itemName = (TextView) itemView.findViewById(R.id.text_itemName);
            this.img_Item = (ImageView) itemView.findViewById(R.id.img_Item);
            this.rel_itemType = (ConstraintLayout) itemView.findViewById(R.id.rel_itemType);
        }
    }



    public DrawerAdapter(ArrayList<DrawerModel>data, Context context) {
        this.dataSet = data;
        this.mContext = context;
        total_types = dataSet.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        switch (viewType) {
            case DrawerModel.HEADER_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.drawer_header, parent, false);
                return new HeaderTypeViewHolder(view);
            case DrawerModel.ITEM_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.drawer_item, parent, false);
                return new ItemTypeViewHolder(view);

        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {

        switch (dataSet.get(position).type) {
            case 0:
                return DrawerModel.HEADER_TYPE;
            case 1:
                return DrawerModel.ITEM_TYPE;

            default:
                return -1;
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int listPosition) {

        final DrawerModel object = dataSet.get(listPosition);
        if (object != null) {
            switch (object.type) {
                case DrawerModel.HEADER_TYPE:
                    ((HeaderTypeViewHolder) holder).text_userName.setText(object.text);
                    String fburl =PrefHelper.getStoredString(mContext, PrefHelper.PREF_FILE_NAME, StaticConstants.FACEBOOK_USER_IMAGE);
                    Picasso.with(mContext)
                            .load(fburl)
                            .into(((HeaderTypeViewHolder) holder).img_User);
                    break;
                case DrawerModel.ITEM_TYPE:
                    ((ItemTypeViewHolder) holder).text_itemName.setText(object.text);
                    ((ItemTypeViewHolder) holder).img_Item.setImageResource(object.data);
                    ((ItemTypeViewHolder) holder).rel_itemType.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if(object.text.equalsIgnoreCase(StaticConstants.MENU_CURRENT_WEATHER)){
                                ((BaseFragmentActivity)mContext).OnCurrentWeatherFragment(true);

                            }
                            else  if(object.text.equalsIgnoreCase(StaticConstants.MENU_FORECAST)){
                                ((BaseFragmentActivity)mContext).OnForecastWeatherFragment(true);

                            }
                            else  if(object.text.equalsIgnoreCase(StaticConstants.MENU_LOGOUT)){
                                ((BaseFragmentActivity)mContext).LogoutCall();

                            }

                        }
                    });
                    break;

            }
        }
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}
