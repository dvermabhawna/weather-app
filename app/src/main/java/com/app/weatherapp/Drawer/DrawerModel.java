package com.app.weatherapp.Drawer;

/**
 * Created by Bhawna on 3/26/2018.
 */

public class DrawerModel {
    public static final int HEADER_TYPE=0;
    public static final int ITEM_TYPE=1;

    public int type;
    public int data;
    public String text;

    public DrawerModel(int type, String text, int data)
    {
        this.type=type;
        this.data=data;
        this.text=text;

    }
}
