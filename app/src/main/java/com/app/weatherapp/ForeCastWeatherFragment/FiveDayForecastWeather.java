package com.app.weatherapp.ForeCastWeatherFragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.weatherapp.AsyncTask.AuthCommanTask;
import com.app.weatherapp.BaseFragmentActivity;
import com.app.weatherapp.Constants.StaticConstants;
import com.app.weatherapp.Interface.OnBackPressListener;
import com.app.weatherapp.Network.BaseNetwork;
import com.app.weatherapp.Interface.IAsyncTaskRunner;
import com.app.weatherapp.Network.ResultMessage;
import com.app.weatherapp.Network.WeatherService;
import com.app.weatherapp.R;
import com.app.weatherapp.Util.TrackGPS;
import com.app.weatherapp.Util.Util;

import java.util.HashMap;

/**
 * Created by Bhawna on 3/26/2018.
 */

public class FiveDayForecastWeather extends Fragment implements OnBackPressListener,
        View.OnClickListener, IAsyncTaskRunner {


    private LayoutInflater inflater;
    private Context context;
    private WeatherService mWeatherService;
    private TrackGPS gps;
    private double mLongitude;
    private double mLatitude;
    private ListView list_forecast;
    private ProgressBar progress_bar;
    private ForecastAdapter adapter_forecast;
    private TextView text_currentCity;
    private TextView text_currentTime;

    @Override
    public View onCreateView(final LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_fivedayforecast,
                container, false);
        this.inflater = inflater;

        initView(view);
        return view;
    }

    private void initView(View view) {
        GetLocation();
        list_forecast = (ListView) view.findViewById(R.id.list_forecast);
        text_currentTime = (TextView) view.findViewById(R.id.text_currentTime);
        progress_bar = (ProgressBar) view.findViewById(R.id.progress_bar);
        text_currentCity = (TextView) view.findViewById(R.id.text_currentCity);
        text_currentTime.setText(Util.getCurrentTime());
        if (Util.isNetworkAvailable(context)){
            progress_bar.setVisibility(View.VISIBLE);
            getFiveDayForecastData();
        }
        else{
            progress_bar.setVisibility(View.GONE);
            Toast.makeText(context, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
        }

    }

    public void getFiveDayForecastData() {
        HashMap<String, String> mHashMap = new HashMap<String, String>();
        mHashMap.put(StaticConstants.JSON_USER_LATTITUDE, String.valueOf(mLatitude)
        );
        mHashMap.put(StaticConstants.JSON_USER_LONGITUDE, String.valueOf(mLongitude));
        mHashMap.put(StaticConstants.JSON_USER_APPID, BaseNetwork.obj().APP_ID);
        mHashMap.put(StaticConstants.JSON_FORECAST_UNIT, "metric");
        AuthCommanTask<HashMap<String, String>, ResultMessage> task = new AuthCommanTask<HashMap<String, String>, ResultMessage>(
                context, this,
                BaseNetwork.obj().KEY_FORECAST, progress_bar, StaticConstants.GET_METHOD);
        task.execute(mHashMap);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public boolean onBackPressed() {
        ((BaseFragmentActivity) context).OnCurrentWeatherFragment(false);
        return true;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }


    private void GetLocation() {
        gps = new TrackGPS(context);
        if (gps != null) {
            if (gps.canGetLocation()) {
                mLongitude = gps.getLongitude();
                mLatitude = gps.getLatitude();

            } else {
                gps.showSettingsAlert();
            }
        }


    }

    @Override
    public void taskStarting() {

    }

    @Override
    public void taskCompleted(Object o) {
        ResultMessage message = (ResultMessage) o;
        if (message.TYPE.equalsIgnoreCase(StaticConstants.FORECAST_LIST_DATA)) {
            adapter_forecast = new ForecastAdapter(context, message.forecastlists);
            list_forecast.setAdapter(adapter_forecast);
            text_currentCity.setText(message.currentCity);
        }

    }

    @Override
    public void taskProgress(String urlString, Object o) {

    }

    @Override
    public void taskErrorMessage(Object o) {

    }
}
