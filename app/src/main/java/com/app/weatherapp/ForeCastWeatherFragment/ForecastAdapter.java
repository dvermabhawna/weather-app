package com.app.weatherapp.ForeCastWeatherFragment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.weatherapp.R;
import com.app.weatherapp.Util.Util;

import java.util.ArrayList;

/**
 * Created by Bhawna on 3/26/2018.
 */

public class ForecastAdapter   extends BaseAdapter  {

    private LayoutInflater li;
    private Context context;
    private ArrayList<WeatherDataForecastlist> items;

    public ForecastAdapter(Context con, ArrayList<WeatherDataForecastlist> result
    ) {
        this.context = con;
        this.items = (ArrayList<WeatherDataForecastlist>) result;
        li = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = li.inflate(R.layout.fragment_forecast_list_item, parent, false);
            holder = new ViewHolder();
            holder.text_currentDateTime = (TextView) convertView.findViewById(R.id.text_currentDateTime);
            holder.text_currentTemp = (TextView) convertView.findViewById(R.id.text_currentTemp);
            holder.text_currentTime = (TextView) convertView.findViewById(R.id.text_currentTime);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.text_currentDateTime.setText(Util.convertIntoDisplayFormat(items.get(position).dt_txt));
        holder.text_currentTime.setText(Util.convertIntoTimeFormat(items.get(position).dt_txt));
        holder.text_currentTemp.setText(items.get(position).temp + " \u2103");

        return convertView;
    }


    static class ViewHolder {
        TextView text_currentDateTime, text_currentTemp,text_currentTime;
    }



}

