package com.app.weatherapp.Interface;
/**
 * Created by Bhawna on 3/26/2018.
 */
import android.content.Context;

public interface IAsyncTaskRunner<Progress, Result> {

	void taskStarting();

	void taskCompleted(Result result);

	void taskProgress(String urlString, Progress progress);

	void taskErrorMessage(Result result);
	
	Context getContext();


}
