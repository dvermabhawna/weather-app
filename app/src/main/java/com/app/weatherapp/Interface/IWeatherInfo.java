package com.app.weatherapp.Interface;

import com.app.weatherapp.CurrentWeatherFragment.WeatherDataModel;

/**
 * Created by Bhawna on 3/26/2018.
 */

public interface IWeatherInfo {
    public void onWeatherData(WeatherDataModel data);

    public void onNoWeatherData();
}
