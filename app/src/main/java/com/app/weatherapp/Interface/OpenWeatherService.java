package com.app.weatherapp.Interface;


import com.app.weatherapp.CurrentWeatherFragment.WeatherDataModel;
import com.app.weatherapp.ForeCastWeatherFragment.WeatherDataForecastlist;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by Bhawna on 3/26/2018.
 */
public interface OpenWeatherService {

    @GET("/data/2.5/weather")
    public void getWeatherData(@Query("lat") String mLattitude,@Query("lon") String mLongitude, @Query("units") String unit,
                               @Query("APPID") String appid,
                               Callback<WeatherDataModel> onWeatherData);

    @GET("/data/2.5/forecast")
    public void getWeatherForecastData(@Query("lat") String mLattitude,@Query("lon") String mLongitude, @Query("units") String unit,
                               @Query("APPID") String appid,
                               Callback<WeatherDataForecastlist> onWeatherData);
}
