package com.app.weatherapp;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.app.weatherapp.Constants.PrefHelper;
import com.app.weatherapp.Constants.StaticConstants;
import com.app.weatherapp.Util.Logger;
import com.app.weatherapp.Util.TrackGPS;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Bhawna on 3/26/2018.
 */

public class LoginActivity extends Activity implements View.OnClickListener {

    private ImageView img_facebookLogin;
    private CallbackManager callbackManager;
    public static final int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 123;
    public static final int REQUEST_CODE_GPS_LOCATION = 124;
    private TrackGPS gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);
        initview();

    }

    private void initview() {
        img_facebookLogin = (ImageView) findViewById(R.id.img_facebookLogin);
        img_facebookLogin.setOnClickListener(this);
        if (!checkPermission())
            requestPermission();
        else
            GetLocation();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_facebookLogin:
                loginToFacebook();
                break;
        }
    }

    private void loginToFacebook() {

        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "user_photos", "public_profile"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(
                                            JSONObject json,
                                            GraphResponse response) {
                                        String jsonresult = String.valueOf(json);
                                        System.out.println("JSON Result" + jsonresult);

                                        try {

                                            System.out.println("JSON Result" + jsonresult);
                                            String str_email = json.getString("email");
                                            String str_id = json.getString("id");
                                            String name = json.getString("name");
                                            String picture = json.getString("picture");
                                            JSONObject pictireObject = new JSONObject(picture);
                                            JSONObject data = pictireObject.getJSONObject("data");
                                            String url = data.getString("url");
                                            PrefHelper.storeString(LoginActivity.this,
                                                    PrefHelper.PREF_FILE_NAME, StaticConstants.FACEBOOK_USER_ID, str_id);
                                            PrefHelper.storeString(LoginActivity.this,
                                                    PrefHelper.PREF_FILE_NAME, StaticConstants.FACEBOOK_USER_NAME, name);
                                            PrefHelper.storeString(LoginActivity.this,
                                                    PrefHelper.PREF_FILE_NAME, StaticConstants.FACEBOOK_USER_IMAGE, url);
                                            SignUpSuccessfull();
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,picture,location");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Logger.d("Facebook--Error", "::" + error.toString());
                    }
                });
    }

    public void SignUpSuccessfull() {
        LoginManager.getInstance().logOut();
        startActivity(new Intent(LoginActivity.this, BaseFragmentActivity.class));
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_GPS_LOCATION) {

            if(resultCode==0)
                GetLocation();

        } else
            callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int result2 = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int result3 = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION);
        int result4 = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_NETWORK_STATE);

        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED
                && result2 == PackageManager.PERMISSION_GRANTED && result3 == PackageManager.PERMISSION_GRANTED
                && result4 == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_NETWORK_STATE},
                REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS:
                if (grantResults.length > 0) {

                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean storageAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean readstorageAccepted = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    boolean coarseLocationAccepted = grantResults[3] == PackageManager.PERMISSION_GRANTED;
                    boolean networkAccepted = grantResults[4] == PackageManager.PERMISSION_GRANTED;

                    if (locationAccepted && storageAccepted && readstorageAccepted && coarseLocationAccepted && networkAccepted)
                        GetLocation();
                        // Toast.makeText(this, "Permission Granted, Now you can access location and store data.",Toast.LENGTH_SHORT).show();
                    else {
                        // Toast.makeText(this, "Permission Denied, You cannot access location data and store data..",Toast.LENGTH_SHORT).show();

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                                showMessageOKCancel("You need to allow access to all the permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                                                                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                                                                    , Manifest.permission.READ_EXTERNAL_STORAGE,
                                                                    Manifest.permission.ACCESS_COARSE_LOCATION,
                                                                    Manifest.permission.ACCESS_NETWORK_STATE},
                                                            REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                                                }
                                            }
                                        });
                                return;
                            }
                        }

                    }
                }


                break;
        }
    }


    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private void showMessageOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .create()
                .show();
    }

    private void GetLocation() {
        gps = new TrackGPS(this);
        if (gps != null) {
            if (gps.canGetLocation()) {
                Logger.d("Gps_Enabled", "-----gps enabled--");
            } else {
                showMessageOK("Please enable your GPS location",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivityForResult(intent, REQUEST_CODE_GPS_LOCATION);
                            }
                        });
            }
        }
    }


}
