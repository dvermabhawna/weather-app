package com.app.weatherapp.Network;

import android.app.AlertDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.app.weatherapp.Constants.StaticConstants;
import com.app.weatherapp.R;
import com.app.weatherapp.Util.Logger;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Bhawna on 3/26/2018.
 */


public class BaseNetwork {
	public final String URL_HOST = "http://api.openweathermap.org/data/2.5/";
	public  final String KEY_FORECAST ="forecast?";
	public  final String APP_ID ="c67f106c2a3e3ab9426238f454f68c2f";

	private static BaseNetwork obj = null;
	public int TimeOut = 5000;

	public synchronized static BaseNetwork obj() {
		if (obj == null)
			obj = new BaseNetwork();
		return obj;
	}

	public boolean checkConnOnline(Context _context) {
		ConnectivityManager conMgr = (ConnectivityManager) _context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (conMgr != null) {
			NetworkInfo i = conMgr.getActiveNetworkInfo();
			if (i == null) {
				return false;
			}
			if (!i.isConnected()) {
				return false;
			}
			if (!i.isAvailable()) {
				return false;
			}
			return true;
		} else
			return false;
	}

	public String PostMethodWay(Context context, String KeyWord,
			HashMap<String, String> map, int timeOut,String methodtype) {
		String hostName = URL_HOST + KeyWord;
		HttpClient httpClient = getHttpClient();
		HttpEntity httpEntity = null;
		HttpResponse httpResponse = null;
		HttpPost httpPost = new HttpPost(hostName);
		String response_str = "";
		try {

			if(methodtype.equalsIgnoreCase(StaticConstants.POST_METHOD)){
				List<NameValuePair> postParameters = new ArrayList<NameValuePair>();
				for (String key : map.keySet()) {
					postParameters.add(new BasicNameValuePair(key, map.get(key)));
					Logger.i("key"+key,"map"+ map.get(key));
				}
				httpPost.setEntity(new UrlEncodedFormEntity(postParameters,"UTF-8"));
				httpResponse = httpClient.execute(httpPost);
			}
			else if(methodtype.equalsIgnoreCase(StaticConstants.GET_METHOD)){
				String url=GenrateUrl(KeyWord, map);
				url=url.replaceAll(" ", "%20");
				Logger.i("Complete get url ",url);
				HttpGet httpGet = new HttpGet(url);

				httpResponse = httpClient.execute(httpGet);

			}
			else if(methodtype.equalsIgnoreCase(StaticConstants.DELETE_METHOD)){
				String url=GenrateUrl(KeyWord, map);
				HttpDelete httpGet = new HttpDelete(url);

				httpResponse = httpClient.execute(httpGet);

			}
			httpEntity = httpResponse.getEntity();
			response_str = EntityUtils.toString(httpEntity);
		} catch (SocketException se) {
			se.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return response_str;
	}





	public static final int TIME_OUT_CONNECTION=5*1000;

	public static HttpClient getHttpClient(){
		HttpClient mhttpclient=null;
		if(mhttpclient==null){
			mhttpclient=new DefaultHttpClient(); 
			final HttpParams mhttpparams=mhttpclient.getParams();
			HttpConnectionParams.setSoTimeout(mhttpparams, TIME_OUT_CONNECTION);
			HttpConnectionParams.setConnectionTimeout(mhttpparams, TIME_OUT_CONNECTION);
		}
		return mhttpclient;
	}
	


	public void showErrorNetworkDialog(final Context c, String message) {

		View layout = ((LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).
				inflate(R.layout.dialog_layout, null, false);
		AlertDialog.Builder builder = new AlertDialog.Builder(c);
		builder.setView(layout);
		Button btn = (Button) layout.findViewById(R.id.btn_ok);
		TextView text = (TextView) layout.findViewById(R.id.text_message);
		text.setText(message);
		final AlertDialog alert = builder.create();
		alert.show();
		btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				alert.cancel();
			}
		});
		/*AlertDialog.Builder builder = new AlertDialog.Builder(c);
		builder.setMessage(message).setNegativeButton("Ok",
				new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();*/
	}







	public String GenrateUrl(String action,
			HashMap<String, String> mHashMap) {
		String url = URL_HOST+action;
		String containpart = "";
		try {

			for (String key : mHashMap.keySet()) {
				Logger.i(":: " + key, ":: " + mHashMap.get(key));
				if (!TextUtils.isEmpty(containpart)){
					String value = mHashMap.get(key);
					String encoded = URLEncoder.encode(value, "UTF-8");
					containpart += "&" + key + "=" + encoded;
				}

				else{
					String value = mHashMap.get(key);

					String encoded = URLEncoder.encode(value, "UTF-8");
					containpart = key + "=" + encoded;
				}

			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return url + containpart;

	}

}
