package com.app.weatherapp.Network;


import com.app.weatherapp.ForeCastWeatherFragment.WeatherDataForecastlist;

import java.util.ArrayList;
/**
 * Created by Bhawna on 3/26/2018.
 */
public class ResultMessage {
	public int STATUS;
	public String RESPONSE;
	public String ERRORMESSAGE;
	public String TYPE;
	public String currentCity;
	public String Error;
	public  ArrayList<WeatherDataForecastlist> forecastlists=new ArrayList<>();
}
