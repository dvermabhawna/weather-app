package com.app.weatherapp.Network;

import android.content.ContentValues;
import android.content.Context;
import android.text.TextUtils;

import com.app.weatherapp.Constants.StaticConstants;
import com.app.weatherapp.ForeCastWeatherFragment.WeatherDataForecastlist;
import com.app.weatherapp.Util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Class to handle http response provided by any api for example weather api-forecast
 * Created by Bhawna on 3/26/2018.
 */

public class WeatherAppParser {

    public void ParseForecastData(Context context, ResultMessage resultMessage,
                                       String jsonStr) {
        try {

            JSONObject jsonuser = new JSONObject(jsonStr);
                if (jsonuser.has(StaticConstants.FORECAST_LIST)) {
                    resultMessage.STATUS = StaticConstants.ASYN_RESULT_OK;
                    resultMessage.TYPE = StaticConstants.FORECAST_LIST_DATA;
                        JSONArray jsonDrivers = jsonuser.getJSONArray(StaticConstants.FORECAST_LIST);
                        for (int k = 0; k < jsonDrivers.length(); k++) {
                            WeatherDataForecastlist item=new WeatherDataForecastlist();
                            JSONObject object = jsonDrivers.getJSONObject(k);
                            String time = object.getString(StaticConstants.JSON_DATE_TIME);
                            item.dt_txt=time;
                            JSONObject main=object.getJSONObject(StaticConstants.FORECAST_MAIN);
                            String temp = main.getString(StaticConstants.JSON_TEMP);
                            item.temp=temp;
                           resultMessage.forecastlists.add(item);
                        }
                    JSONObject cityObject=jsonuser.getJSONObject(StaticConstants.FORECAST_CITY);
                    String cityName = cityObject.getString(StaticConstants.JSON_NAME);
                    resultMessage.currentCity=cityName;

                } else {
                        resultMessage.STATUS = StaticConstants.ASYN_RESULT_CANCEL;
                        resultMessage.ERRORMESSAGE = jsonuser.getString("message");
                }


        } catch (JSONException exception) {
            exception.printStackTrace();
            resultMessage.STATUS = StaticConstants.ASYN_RESULT_PARSE_FAILED;
        }
    }
}
