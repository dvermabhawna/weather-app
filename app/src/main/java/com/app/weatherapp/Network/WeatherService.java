package com.app.weatherapp.Network;


import com.app.weatherapp.CurrentWeatherFragment.WeatherDataModel;
import com.app.weatherapp.Interface.IWeatherInfo;
import com.app.weatherapp.Interface.OpenWeatherService;
import com.app.weatherapp.Util.Logger;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Weather service that uses Retrofit to get the weather data
 *Created by Bhawna on 3/26/2018.
 */
public class WeatherService {

    private IWeatherInfo mListener;

    public WeatherService(IWeatherInfo listener) {
        this.mListener = listener;
    }

    public void getWeather(String mLattitude,String mLongitude,String unit) {
        RestAdapter adapter = new RestAdapter.Builder().setEndpoint("http://api.openweathermap.org").build();
        OpenWeatherService openWeatherService = adapter.create(OpenWeatherService.class);

        openWeatherService.getWeatherData(mLattitude,mLongitude, unit,"c67f106c2a3e3ab9426238f454f68c2f", new Callback<WeatherDataModel>() {
            @Override
            public void success(WeatherDataModel weatherDataModel, Response response) {
                mListener.onWeatherData(weatherDataModel);
                Logger.d("Retrofit--Response", "::" + response);
            }

            @Override
            public void failure(RetrofitError error) {
                mListener.onNoWeatherData();
                Logger.d("Retrofit--Error", "::" + error);
            }
        });
    }


}
