package com.app.weatherapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.preference.Preference;
import android.text.TextUtils;
import android.view.Window;

import com.app.weatherapp.Constants.PrefHelper;
import com.app.weatherapp.Constants.StaticConstants;

/**
 * Created by Bhawna on 3/26/2018.
 */

public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                if(TextUtils.isEmpty(PrefHelper.
                        getStoredString(SplashActivity.this,PrefHelper.PREF_FILE_NAME, StaticConstants.FACEBOOK_USER_ID)))
                {
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    finish();
                }
                else {
                    startActivity(new Intent(SplashActivity.this, BaseFragmentActivity.class));
                    finish();
                }



            }
        }, 2000);


    }
}
