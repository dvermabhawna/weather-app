package com.app.weatherapp.Ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;
import com.app.weatherapp.R;
/**
 * Created by Bhawna on 3/26/2018.
 */

public class CustomTextView extends TextView {
	private static final String FuturaMedium = "FuturaMedium";



	public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		if (!isInEditMode())
			init(attrs);

	}

	public CustomTextView(Context context, AttributeSet attrs) {
		super(context, attrs);

		if (!isInEditMode())
			init(attrs);

	}

	public CustomTextView(Context context) {
		super(context);

		if (!isInEditMode())
			init(null);
	}

	private void init(AttributeSet attrs) {
		// TODO Auto-generated method stub
		if (attrs != null) {
			TypedArray a = getContext().obtainStyledAttributes(attrs,
					R.styleable.CustomTextView);
			String fontName = a.getString(R.styleable.CustomTextView_fontName);
			if (FuturaMedium.equals(fontName)) {
				Typeface myTypeface = Typeface.createFromAsset(getContext()
						.getAssets(), "fonts/" + "FuturaMedium.ttf");
				setTypeface(myTypeface);
			}
			a.recycle();
		}

	}
}

