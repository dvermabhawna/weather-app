package com.app.weatherapp.Ui;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.app.weatherapp.Interface.ILogoutConfirm;
import com.app.weatherapp.R;

/**
 * Created by Bhawna on 3/27/2018.
 */

public class LogoutDialog  extends Dialog {

    Context context;
    ILogoutConfirm callBackFinish;
    public LogoutDialog(Context context,ILogoutConfirm  callBackFinish)

    {
        super(context);
        this.callBackFinish=callBackFinish;
        this.context=context;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_logout);

        Button yes=(Button)findViewById(R.id.btn_yes);
        Button no=(Button)findViewById(R.id.btn_no);




        yes.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();

                callBackFinish.LogMeOut();
            }

        });

        no.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();

            }
        });
    }


}
