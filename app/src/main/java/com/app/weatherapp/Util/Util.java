package com.app.weatherapp.Util;
/**
 * Created by Bhawna on 3/26/2018.
 */


import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.util.Log;

import com.app.weatherapp.BaseFragmentActivity;
import com.app.weatherapp.Constants.PrefHelper;
import com.app.weatherapp.Constants.StaticConstants;
import com.app.weatherapp.Drawer.DrawerModel;
import com.app.weatherapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class Util {

    public static final String FORECAST_DATE_FORMAT = "yyyy-MM-dd kk:mm:ss";
    public static final String DISPLAY_DATE_FORMAT = "EEE dd MMMM yyyy";
    public static final String DISPLAY_TIME_FORMAT = "hh:mm a";
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();

            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    Log.i("Class", info[i].getState().toString());
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    public static String CheckJsonIsEmpty(JSONObject jsonObject,
                                          String jsonTagname) throws JSONException {
        if (jsonObject.has(jsonTagname))
            return TextUtils.isEmpty(jsonObject.getString(jsonTagname)) ? ""
                    : jsonObject.getString(jsonTagname);
        else
            return "";

    }
    public static String convertIntoDisplayFormat(String dateString) {
        String tDate;
        try {
            Date date = new SimpleDateFormat(FORECAST_DATE_FORMAT).parse(dateString);
           //String dateFormat = "yyyy-MM-dd kk:mm:ss";
            SimpleDateFormat sdf = new SimpleDateFormat(DISPLAY_DATE_FORMAT, new Locale("en_US"));
            tDate = sdf.format(date);
        } catch (ParseException e) {
            tDate = dateString;
            e.printStackTrace();
        }
        return tDate;
    }

    public static String convertIntoTimeFormat(String dateString) {
        String tDate;
        try {
            Date date = new SimpleDateFormat(FORECAST_DATE_FORMAT).parse(dateString);
            //String dateFormat = "yyyy-MM-dd kk:mm:ss";
            SimpleDateFormat sdf = new SimpleDateFormat(DISPLAY_TIME_FORMAT, new Locale("en_US"));
            tDate = sdf.format(date);
        } catch (ParseException e) {
            tDate = dateString;
            e.printStackTrace();
        }
        return tDate;
    }

    public static String getCurrentTime(){
        DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
        Date date = new Date();
        return  dateFormat.format(date);
    }

    public static String getCurrentDateTime(){
        DateFormat dateFormat = new SimpleDateFormat("EEE dd MMMM yyyy hh:mm a");
        Date date = new Date();
        return  dateFormat.format(date);
    }

    public static ArrayList<DrawerModel> getModuleItems(Context context) {
        ArrayList<DrawerModel> modulelist = new ArrayList<>();
        modulelist.add(new DrawerModel(DrawerModel.HEADER_TYPE, PrefHelper.
                getStoredString(context, PrefHelper.PREF_FILE_NAME, StaticConstants.FACEBOOK_USER_NAME),0));
        modulelist.add(new DrawerModel(DrawerModel.ITEM_TYPE,StaticConstants.MENU_CURRENT_WEATHER, R.drawable.todayweather));
        modulelist.add(new DrawerModel(DrawerModel.ITEM_TYPE,StaticConstants.MENU_FORECAST,R.drawable.forcast));
        modulelist.add(new DrawerModel(DrawerModel.ITEM_TYPE,StaticConstants.MENU_LOGOUT,R.drawable.logout));
        return modulelist;
    }

    public static String getVersionNumber(Context context){
        String versionName = "";
        try {
            versionName = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            versionName="1.0";
        }
        return versionName;
    }
}
