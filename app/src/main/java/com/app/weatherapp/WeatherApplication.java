package com.app.weatherapp;

import android.app.Application;

import org.acra.ACRA;
import org.acra.ReportField;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

/**
 * Created by Bhawna on 3/27/2018.
 */
@ReportsCrashes(formKey = "", mailTo = "bhawna.v389@gmail.com", customReportContent = {
        ReportField.APP_VERSION_CODE, ReportField.APP_VERSION_NAME,
        ReportField.ANDROID_VERSION, ReportField.PHONE_MODEL,
        ReportField.CUSTOM_DATA, ReportField.STACK_TRACE, ReportField.LOGCAT },
        forceCloseDialogAfterToast = false, mode = ReportingInteractionMode.TOAST, resToastText = R.string.crash_toast_text)
public class WeatherApplication extends Application {
    private static WeatherApplication singleton;
    public static WeatherApplication getApp() {
        return singleton;
    }

    public WeatherApplication() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ACRA.init(this);
        singleton = this;

    }



}
